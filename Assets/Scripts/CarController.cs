﻿using UnityEngine;
using System.Collections;

public class CarController : AIController {

	public GameObject route;
	public float force;
	public Vector3 target;
	public Vector3 dir;
	
	public override void Start ()
	{
		base.Start ();
		RunTree ("drive");
		SetGameObject ("route", route);
	}
	
	// Update is called once per frame
	void Update () {
		target = GetTarget ("next");
		target.y = transform.parent.transform.position.y;
		dir = target - transform.position;
		dir = dir.normalized;
		//transform.position = target;// Vector3.Lerp (transform.position, target, Time.deltaTime);
		transform.parent.rigidbody.AddForce(dir * force * Time.deltaTime);
	}


}
