﻿using UnityEngine;
using System.Collections;

public class ShowFPS : MonoBehaviour
{
	float deltaTime = 0.0f;
	
	void Update()
	{
		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
	}
	
	void OnGUI()
	{
		int w = Screen.width/6;
		int h = Screen.height/6;
		
		GUIStyle style = new GUIStyle();
		

		style.alignment = TextAnchor.MiddleCenter;
		style.fontSize = h /5;
		style.normal.textColor = new Color (0.0f, 0.0f, 0.5f, 1.0f);
		//style.alignment = TextAlignment.Center;
		float msec = deltaTime * 1000.0f;
		float fps = 1.0f / deltaTime;
		string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);

		Rect rect = new Rect(w, 0, w, h*2);
		GUI.Label(rect, text, style);
		rect = new Rect(w*4, 0, w, h*2);
		GUI.Label(rect, text, style);
	}
}